jQuery(document).ready(function($){
	// Gumby is ready to go
	Gumby.ready(function() {
		Gumby.log('Gumby is ready to go...', Gumby.dump());

		// placeholder polyfil
		if(Gumby.isOldie || Gumby.$dom.find('html').hasClass('ie9')) {
			$('input, textarea').placeholder();
		}

		// skip link and toggle on one element
		// when the skip link completes, trigger the switch
		$('#skip-switch').on('gumby.onComplete', function() {
			$(this).trigger('gumby.trigger');
		});

	// Oldie document loaded
	}).oldie(function() {
	// Touch devices loaded
	}).touch(function() {
		
	});
});
