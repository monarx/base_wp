<?php
/** functions.php
 * Base functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Shiliko
 * @since Shiliko 1.0
 */
if ( ! function_exists( 'base_setup' ) ):
	function base_setup() {
		require_once( get_template_directory() . '/functions/widgets.php' );

		register_nav_menus( array(
			'navigation'		=>	__( 'Main Navigation'),
			'header-menu'  	=>	__( 'Header Menu'),
			'footer-menu' 	=>	__( 'Footer Menu')
		) );
	}
endif;
add_action( 'after_setup_theme', 'base_setup' );

/**
 * Registration of theme scripts and styles
 *
 * @author	Shilyaev Igor
 * @since	
 *
 * @return	void
 */
function base_register_scripts_styles() {
		/**
		 * Scripts
		 */
		wp_register_script(
			'modernizr',
			get_template_directory_uri() . "/js/libs/modernizr-2.6.2.min.js",
			array('jquery'),
			true
		);

		wp_register_script(
			'gumby',
			get_template_directory_uri() . "/js/libs/gumby.js",
			array('jquery'),
			'1',
			true
		);

		wp_register_script(
			'gumby-retina',
			get_template_directory_uri() . "/js/libs/ui/gumby.retina.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-fixed',
			get_template_directory_uri() . "/js/libs/ui/gumby.fixed.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-skiplink',
			get_template_directory_uri() . "/js/libs/ui/gumby.skiplink.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-toggleswitch',
			get_template_directory_uri() . "/js/libs/ui/gumby.toggleswitch.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-checkbox',
			get_template_directory_uri() . "/js/libs/ui/gumby.checkbox.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-radiobtn',
			get_template_directory_uri() . "/js/libs/ui/gumby.radiobtn.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-tabs',
			get_template_directory_uri() . "/js/libs/ui/gumby.tabs.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-navbar',
			get_template_directory_uri() . "/js/libs/ui/gumby.navbar.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-validation',
			get_template_directory_uri() . "/js/libs/ui/jquery.validation.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(
			'gumby-init',
			get_template_directory_uri() . "/js/libs/gumby.init.js",
			array('gumby'),
			'1',
			true
		);

		wp_register_script(  //custom scripts.
			'main',
			get_template_directory_uri() . "/js/main.js",
			array('gumby'),
			'1',
			true
		);


		/**
		 * Styles
		 */
		wp_register_style(
			'gumby',
			get_template_directory_uri() . "/css/gumby.css",
			array(),
			'1'
		);
		wp_register_style(
			'main',
			get_template_directory_uri() . "/css/main.css",
			array(),
			'1'
		);
}
add_action( 'init', 'base_register_scripts_styles' ); 

function base_print_scripts_and_styles() {
	/**
		 * Styles
		 */
	wp_enqueue_style( 'gumby' );
	wp_enqueue_style( 'main' );

	/**
		 * scripts
		 */
	wp_enqueue_script( 'modernizr' );
	wp_enqueue_script( 'gumby' );
	wp_enqueue_script( 'gumby-retina' );
	wp_enqueue_script( 'gumby-init' );
	wp_enqueue_script( 'gumby-navbar' );
	wp_enqueue_script( 'gumby-toggleswitch' );
	wp_enqueue_script( 'gumby-skiplink' );
	wp_enqueue_script( 'main' );
}
add_action( 'wp_enqueue_scripts', 'base_print_scripts_and_styles' );

/**
 * Register the sidebars.
 *
 * @author	Konstantin Obenland
 * @since	1.0.0 - 05.02.2012
 *
 * @return	void
 */
function the_bootstrap_widgets_init() {
}

?>