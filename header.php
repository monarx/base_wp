﻿<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Shiliko
 * @since Shiliko 1.0
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if !(IE 7) & !(IE 8) & !(IE 9)]><!-->
	<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header">
	</div>
	<?php endif; ?>

	<header id="header" class="site-header" role="banner">
		<div class="navbar" id="nav1">
			<div class="row">
				<a class="toggle" gumby-trigger="#nav1 > .row > ul" href="#"><i class="icon-menu"></i></a>
				<h1 class="four columns logo">
					<a href="/">
						<img src="<?php echo get_template_directory_uri() . '/images/nexton/nexton-logo_white.png';?>" gumby-retina />
					</a>
				</h1>

				<?php
					wp_nav_menu( array(
						'container' => '',
						'theme_location'	=>	'navigation',
						'menu_class'		=>	'navigation-menu eight columns',
						'depth'				=>	3,
						'fallback_cb'		=>	false
					) ); 
				?>
			</div>
		</div>
	</header>

<main id="main" class="site-main">
