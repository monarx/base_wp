<?php

/**
 * Register the sidebars.
 *
 * @author	Konstantin Obenland
 * @since	1.0.0 - 05.02.2012
 *
 * @return	void
 */
function base_widgets_init() {
	register_sidebar( array(
		'name'					=>	'Before content widget area',
		'id'						=>	'before-content',
		'before_widget'	=>	'<div id="%1$s" class="twelve columns widget %2$s">',
		'after_widget'	=>	'</div>',
		'before_title'	=>	'<div class="widget-title">',
		'after_title'		=>	'</div>',
	) );

	register_sidebar( array(
		'name'					=>	'After content widget area',
		'id'						=>	'after-content',
		'before_widget'	=>	'<div id="%1$s" class="twelve columns widget %2$s">',
		'after_widget'	=>	'</div>',
		'before_title'	=>	'<div class="widget-title">',
		'after_title'		=>	'</div>',
	) );

	register_sidebar( array(
		'name'					=>	'Right Sidebar',
		'id'						=>	'main',
		'before_widget'	=>	'<div id="%1$s" class="widget %2$s">',
		'after_widget'	=>	'</div>',
		'before_title'	=>	'<div class="widget-title">',
		'after_title'		=>	'</div>',
	) );

	register_sidebar( array(
		'name'					=>	'Footer widget area',
		'id'						=>	'footer-before',
		'before_widget'	=>	'<div id="%1$s" class="four columns widget %2$s">',
		'after_widget'	=>	'</div>',
		'before_title'	=>	'<div class="widget-title">',
		'after_title'		=>	'</div>',
	) );

}
add_action( 'widgets_init', 'base_widgets_init' );
?>