<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
	</div><!-- #main -->
</main><!-- #page -->

<footer id="footer">
	<?php get_sidebar( 'footer' ); ?>

	<div class="row">
		<div class="eight columns">
			<?php //get_sidebar( 'footer' ); ?>
		</div>
		<div class="four columns">
			<div class="nexton text-right">
				<a href="http://nexton.ru" title="Создание сайта" target="_blank">
					Создание сайта				
				</a> 
				— 
				<a href="http://nexton.ru" target="_blank">
					<img alt="Создание сайта" src="<?php echo get_template_directory_uri() . '/images/nexton/nexton-logo_white.png';?>">
				</a>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>